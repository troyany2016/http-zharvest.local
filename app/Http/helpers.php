<?php

    function t($textid){
        $column = DB::table('texts')->where('textid', $textid)->get()->first();

        if(!empty($column)){

            if(Auth::check()){
                $html = '';
                $html .= '<span class="admin_edit">';
                $html .= '<span class="admin_edit_link"><a href="'.route('text_edit', array('id' => $column->id)).'" class="edit_block"><i class="fa fa-pencil" aria-hidden="true"></i></a></span>';
                $html .= $column->simpletext;
                $html .= '</span>';
                return $html;
            }
            return $column->simpletext;

        }else{
            if(Auth::check()){
                $html = '';
                $html .= '<span class="admin_edit">';
                $html .= '<span class="admin_edit_link"><a href="'.route('text_list').'?textid='.$textid.'" class="edit_block"><i class="fa fa-plus" aria-hidden="true"></i></a></span>';
                $html .= 'text id "' . $textid . '"';
                $html .= '</span>';
                return $html;
            }

            return 'text id "' . $textid . '"';
        }
    }
    function block($block_id){
        $column = DB::table('blocks')->where('blockid', $block_id)->get()->first();
        if(is_object($column)){
            if(!empty($column->text)){
                if(Auth::check()){
                    $html = '';
                    $html .= '<div class="admin_edit">';
                        $html .= '<div class="admin_edit_link"><a href="'.route('block_edit', array('id' => $column->id)).'" class="edit_block"><i class="fa fa-pencil" aria-hidden="true"></i></a></div>';
                    $html .= $column->text;
                    $html .= '</div>';
                    return $html;
                }
                return $column->text;

            }
        }else{
            return 'block id "' . $block_id . '"';
        }
    }
//    function get_main_menu(){
//       $menu = [];
//       $menus_level1 = App\Menu::get_first_level();
//        foreach ($menus_level1 as $k1 => $menu1) {
//            $menu[] = $menu1;
//            if(App\Menu::get_children($menu1['id'])){
//                $menus_level2 = App\Menu::get_children($menu1['id']);
//                $menu[$k1]['children'] = $menus_level2;
//                foreach ($menus_level2 as $k2 => $menu2){
//                    if(App\Menu::get_children($menu2['id'])){
//                        $menus_level3 = App\Menu::get_children($menu2['id']);
//                        $menu[$k1]['children'][$k2]['children'] = $menus_level3;
//                    }
//                }
//            }
//        }
//       return $menu;
//    }
