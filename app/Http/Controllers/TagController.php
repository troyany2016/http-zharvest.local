<?php

namespace App\Http\Controllers;

use App\Tag;
use Validator;
use Illuminate\Http\Request;

class TagController extends Controller
{
    //
    public function all(Request $request){

        if($request->isMethod('post')){

            $validator = Validator::make($request->all(), ['tag' => 'required']);

            if($validator->fails()){
                return redirect()->route('tag_list')->withInput()->withErrors($validator);
            }
            $res = Tag::insert([
                'name' => $request->tag
            ]);
            if($res){
                return redirect()->route('tag_list')->with('status', 'Tag addded!');
            }
        }

        $allTag = Tag::all();
        $data = [
            'page_title' => 'Edit tags',
            'tags' => $allTag
        ];
        return view('auth.admin.tags_list', $data);
    }
    public function edit(Request $request, $id){
        $tag = Tag::find($id);
        $data = [
            'page_title' => 'Edit tag',
            'tag' => $tag
        ];
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), ['tag' => 'required']);

            if($validator->fails()){
                return redirect()->route('tag_edit', array('id' => $id))->withInput()->withErrors($validator);
            }
            $res = $tag->update([
                'name' => $request->tag
            ]);
            if($res){
                return redirect()->route('tag_list')->with('status', 'tag edited');

            }
        }



        return view('auth.admin.tags_list_edit', $data);
    }
    public function delete($id){

        $cTag = Tag::find($id);
//        dump($cTag->articles);
        if($cTag->articles){
            foreach ($cTag->articles as $artl){
                $artl->tags()->detach($id);
            }
        }
        $res = $cTag->delete();



        if($res){
            return redirect()->route('tag_list')->with('status', "Tag was deleted");
        }else{
            return redirect()->route('tag_list');
        }
    }
}
