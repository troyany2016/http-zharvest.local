<?php

namespace App\Http\Controllers;

use App\Text;
use Validator;
use Illuminate\Http\Request;

class TextController extends Controller
{
    //
    public function all(Request $request){
        $data = Text::all();
        if($request->isMethod('post')){
            $rule = [
                'simpletext' => 'required',
                'textid'    => 'required|unique:texts,textid,'
            ];

            Validator::make($request->all(), $rule)->validate();
            $res = Text::insert([
                'simpletext' => $request->simpletext,
                'textid'    => $request->textid
            ]);

            if($res == true){
                return redirect()->route('text_list')->with('status', "Text '". $request->simpletext ."' was added!");
            }
        }
        return view('auth.admin.texts_list', ['text_list' => $data, 'page_title' => 'Texts list']);
    }

    public function edit($id = null , Request $request){
        $cText = '';
        $cTextId = '';


        if($request->isMethod('get')){
            $cText = Text::find($id)->simpletext;
            $cTextId = Text::find($id)->textid;
        }else{
            $validator = Validator::make($request->all(), [
                'simpletext' => 'required',
                'textid' => 'required|unique:texts,textid,' . $request->id
            ]);

            if ($validator->fails()) {
                return redirect()->route('text_edit', array($id))
                    ->withErrors($validator)
                    ->withInput();
            }else{
                $new_text = Text::find($id)->update([
                    'simpletext' => $request->simpletext,
                    'textid' => $request->textid,

                ]);
                if($new_text){
                    return redirect()->route('text_list')->with('status', "Text was updated");
                }
            }
        }
        return view('auth.admin.texts_list_edit', ['text_id' => $id, 'cText' => $cText, 'cTextId' => $cTextId]);
    }

    public function delete($id){

        $res = Text::destroy($id);

        if($res){
            return redirect()->route('text_list')->with('status', "Text was deleted");
        }else{
            return redirect()->route('text_list');
        }

    }
}
