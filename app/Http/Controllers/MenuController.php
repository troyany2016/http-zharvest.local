<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use Validator;

class MenuController extends Controller
{
    public function all(Request $request){
        $menu = [];
        if($request->isMethod('post')){

            if( $request->menu && !empty($request->menujson)){
                $json = $request->menujson;
                $arr = json_decode($json);
                foreach ($arr as $key=> $itm){
                    $res = Menu::find($itm->id)->update([
                        'weight' => $key,
                        'parent' => 0,
                    ]);
                    if(!empty($itm->children)){
                        $child1 = $itm->children;
                        foreach ($child1 as $kk1 => $ch1){
                            $res = Menu::find($ch1->id)->update([
                                'weight' => $kk1,
                                'parent' => $itm->id,
                            ]);
                            if(!empty($ch1->children)){
                                $child2 = $ch1->children;
                                foreach ($child2 as $kk2 => $ch2) {
                                        $res = Menu::find($ch2->id)->update([
                                        'weight' => $kk2,
                                        'parent' => $ch1->id,
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
            else{
                if($request->add_link){

                    $rule = [
                        'name' => 'required',
                        'name' => 'required',
                        'link'    => 'required|url'
                    ];

                    Validator::make($request->all(), $rule)->validate();

                    $res = Menu::insert([
                        'name' => $request->name,
                        'link'    => $request->link,
                        'parent' => 0,
                        'weight' => 0
                    ]);

                    if($res == true){
                        return redirect()->route('menu_list')->with('status', "Menu item  was added!");
                    }


                }
            }

        }

        $move = '<img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDUxMS42MjYgNTExLjYyNiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTExLjYyNiA1MTEuNjI2OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBhdGggZD0iTTUwNi4xOTksMjQyLjk2OGwtNzMuMDktNzMuMDg5Yy0zLjYxNC0zLjYxNy03Ljg5OC01LjQyNC0xMi44NDgtNS40MjRjLTQuOTQ4LDAtOS4yMjksMS44MDctMTIuODQ3LDUuNDI0ICAgYy0zLjYxMywzLjYxOS01LjQyNCw3LjkwMi01LjQyNCwxMi44NXYzNi41NDdIMjkyLjM1NVYxMDkuNjQxaDM2LjU0OWM0Ljk0OCwwLDkuMjMyLTEuODA5LDEyLjg0Ny01LjQyNCAgIGMzLjYxNC0zLjYxNyw1LjQyMS03Ljg5Niw1LjQyMS0xMi44NDdjMC00Ljk1Mi0xLjgwNy05LjIzNS01LjQyMS0xMi44NTFMMjY4LjY2LDUuNDI5Yy0zLjYxMy0zLjYxNi03Ljg5NS01LjQyNC0xMi44NDctNS40MjQgICBjLTQuOTUyLDAtOS4yMzIsMS44MDktMTIuODUsNS40MjRsLTczLjA4OCw3My4wOWMtMy42MTgsMy42MTktNS40MjQsNy45MDItNS40MjQsMTIuODUxYzAsNC45NDYsMS44MDcsOS4yMjksNS40MjQsMTIuODQ3ICAgYzMuNjE5LDMuNjE1LDcuODk4LDUuNDI0LDEyLjg1LDUuNDI0aDM2LjU0NXYxMDkuNjM2SDEwOS42MzZ2LTM2LjU0N2MwLTQuOTUyLTEuODA5LTkuMjM0LTUuNDI2LTEyLjg1ICAgYy0zLjYxOS0zLjYxNy03LjkwMi01LjQyNC0xMi44NS01LjQyNGMtNC45NDcsMC05LjIzLDEuODA3LTEyLjg0Nyw1LjQyNEw1LjQyNCwyNDIuOTY4QzEuODA5LDI0Ni41ODUsMCwyNTAuODY2LDAsMjU1LjgxNSAgIHMxLjgwOSw5LjIzMyw1LjQyNCwxMi44NDdsNzMuMDg5LDczLjA4N2MzLjYxNywzLjYxMyw3Ljg5Nyw1LjQzMSwxMi44NDcsNS40MzFjNC45NTIsMCw5LjIzNC0xLjgxNywxMi44NS01LjQzMSAgIGMzLjYxNy0zLjYxLDUuNDI2LTcuODk4LDUuNDI2LTEyLjg0N3YtMzYuNTQ5SDIxOS4yN3YxMDkuNjM2aC0zNi41NDJjLTQuOTUyLDAtOS4yMzUsMS44MTEtMTIuODUxLDUuNDI0ICAgYy0zLjYxNywzLjYxNy01LjQyNCw3Ljg5OC01LjQyNCwxMi44NDdzMS44MDcsOS4yMzMsNS40MjQsMTIuODU0bDczLjA4OSw3My4wODRjMy42MjEsMy42MTQsNy45MDIsNS40MjQsMTIuODUxLDUuNDI0ICAgYzQuOTQ4LDAsOS4yMzYtMS44MSwxMi44NDctNS40MjRsNzMuMDg3LTczLjA4NGMzLjYyMS0zLjYyLDUuNDI4LTcuOTA1LDUuNDI4LTEyLjg1NHMtMS44MDctOS4yMjktNS40MjgtMTIuODQ3ICAgYy0zLjYxNC0zLjYxMy03Ljg5OC01LjQyNC0xMi44NDctNS40MjRoLTM2LjU0MlYyOTIuMzU2aDEwOS42MzN2MzYuNTUzYzAsNC45NDgsMS44MDcsOS4yMzIsNS40MiwxMi44NDcgICBjMy42MjEsMy42MTMsNy45MDUsNS40MjgsMTIuODU0LDUuNDI4YzQuOTQ0LDAsOS4yMjYtMS44MTQsMTIuODQ3LTUuNDI4bDczLjA4Ny03My4wOTFjMy42MTctMy42MTcsNS40MjQtNy45MDEsNS40MjQtMTIuODUgICBTNTA5LjgyLDI0Ni41ODUsNTA2LjE5OSwyNDIuOTY4eiIgZmlsbD0iIzAwMDAwMCIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />';
        $first_level = Menu::get_first_level();
        foreach ($first_level as $key =>$item) {
            $menu[] = [
                'id' => $item['id'],
                'content' => $move . ' <a href="'. route('menu_edit', array('id' => $item['id'])) .'">'. $item['name'] .'</a>',
            ];
            $childs = Menu::get_children($item['id']);
            if ($childs) {
                foreach ($childs as $kkch =>$childd) {
                    $menu[$key]['children'][] = [
                        'id' => $childd['id'],
                        'content' => $move . ' <a href="'. route('menu_edit', array('id' => $childd['id'])) .'">'. $childd['name'] .'</a>',
                    ];
                    $childdds = Menu::get_children($childd['id']);
                    if ($childdds) {
                        foreach ($childdds as $childddd) {

                            $menu[$key]['children'][$kkch]['children'][] = [
                                'id' => $childddd['id'],
                                'content' => $move . '   <a href="'. route('menu_edit', array('id' => $childddd['id'])) .'">'. $childddd['name'] .'</a>',
                            ];
                        }
                    }
                }
            }
        }

        $menu = json_encode($menu);
        $data = [
            'page_title' => 'Menu Edit',
            'json' => $menu

        ];

        return view('auth.admin.menu_list', $data);
    }
    public static function get_main_menu(){
        $menu = [];
        $menus_level1 = Menu::get_first_level();
        foreach ($menus_level1 as $k1 => $menu1) {
            $menu[] = $menu1;
            if(Menu::get_children($menu1['id'])){
                $menus_level2 = Menu::get_children($menu1['id']);
                $menu[$k1]['children'] = $menus_level2;
                foreach ($menus_level2 as $k2 => $menu2){
                    if(Menu::get_children($menu2['id'])){
                        $menus_level3 = Menu::get_children($menu2['id']);
                        $menu[$k1]['children'][$k2]['children'] = $menus_level3;
                    }
                }
            }
        }
        return $menu;
    }

    public function edit($id = null, Request $request){
        $current = Menu::find($id);

        $data = [
            'page_title' => 'Edit item',
            'item' => $current
        ];
        if($request->isMethod('post')){

            $rule = [
                'link' => 'required|url',
                'name' => 'required',

            ];

            $validator = Validator::make($request->all(), $rule);

            if ($validator->fails()) {
                return redirect()->route('menu_edit', array('id' => $id))
                    ->withErrors($validator)
                    ->withInput();
            }
//
            $menu = Menu::find($id)->update([
                'name' => $request->name,
                'link' => $request->link,

            ]);

            if($menu){
                return redirect()->route('menu_list')->with('status', "Menu item was updated");
            }


        }


        return view('auth.admin.menu_list_edit', $data);
    }
}
