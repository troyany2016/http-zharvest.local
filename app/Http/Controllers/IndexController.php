<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Photo;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    public function show(){

        $front_article =  Article::all();
        foreach ($front_article as $frontItem) {
            $tags = $frontItem->tags;
            $frontItem['tags'] = $tags;
        }

        $cats = [];
        if($categories = Category::all()){
            $cats =  $categories = Category::all();
        }

        $photgrafs = [];
        $allph = Photo::all();

        if(count($allph)){
            $ind = (int) ceil(count($allph)/2);
            $allph = (array)$allph;
            $allph = array_shift($allph);
            $photgrafs = array_chunk($allph, $ind);

        }

        $data = [
            'front_articles' => $front_article,
            'cats' => $cats,
            'photgrafs' => $photgrafs

        ];

        return view('templates.index', $data);
    }

}
