<?php

namespace App\Http\Controllers;

use App\Block;
use Validator;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    public function all(Request $request){

        $data = Block::paginate(5);
        if($request->isMethod('post')){

           $validator = Validator::make($request->all(),[
                'text' => 'required',
                'blockid' => 'required|unique:blocks,blockid,' . $request->id
           ]);

           if ($validator->fails()){
                return redirect()->route('block_list', array($request->id))
                    ->withErrors($validator)
                    ->withInput();
           }else{
               $blockid = $request->all()['blockid'];
               $text = $request->all()['text'];
               Block::insert([
                   'blockid' => $blockid,
                   'text' => $text
               ]);
               return redirect()->route('block_list')->with('status', "Block '". $blockid ."' was added!");
           }

        }
        return view('auth.admin.block_list', ['block_list' => $data, 'page_title' => 'Block list']);
    }
    public function edit(Request $request, $id = null){
        $cblock = Block::where('id', $id)->get()->first();

        if($request->isMethod('get')){
            $id = $cblock->id;
            $bText = $cblock->text;
            $bTextId = $cblock->blockid;
        }else{
            $message =[
                'text.required' => 'Custommm messaage required !!!!'
            ];

            $validator = Validator::make($request->all(),[
                'text' => 'required',
                'blockid' => 'required|unique:blocks,blockid,' . $request->id
            ], $message );

            if ($validator->fails()) {
                return redirect()->route('block_edit', array($id))
                    ->withErrors($validator)
                    ->withInput();
            }else{
                $new_block = Block::find($id)->update([
                    'text' => $request->text,
                    'blockid' => $request->blockid,

                ]);

                if($new_block){
                    return redirect()->route('block_list')->with('status', "Text was updated");
                }
            }
        }

        return view('auth.admin.block_list_edit', ['block_id' => $id, 'bText' => $bText, 'bTextId' => $bTextId]);

    }

    public function delete($id = null){
        $res = Block::destroy($id);
        if($res){
            return redirect()->route('block_list')->with('status', "Block was deleted");
        }else{
            return redirect()->route('block_list');
        }

    }
}
