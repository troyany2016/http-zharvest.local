<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class ContactController extends Controller
{
    public function show(Request $request){
        $data = [
            'page_title' => 'Contact Us',
            'limited' => true,
            'hide_map' => true
        ];
        if($request->isMethod('post')){
            $rule = [
                'name' => 'required',
                'email' => 'email',
                'message' => 'required'
            ];

            $validator = Validator::make($request->all(), $rule);


            if($request->ajax()){
                if ($validator->passes()) {

                    $res =  Contact::insert([
                        'name' => $request->name,
                        'text' => $request->message,
                        'email' => $request->email,
                        'subject' => $request->subject,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    return response()->json(['success'=>'Added new records.']);
                }

                return response()->json(['error'=>$validator->errors()->all()]);
            }

            if ($validator->fails()) {

                return redirect()->route('contact_page')
                    ->withErrors($validator)
                    ->withInput();
            }
           $res =  Contact::insert([
                'name' => $request->name,
                'text' => $request->message,
                'email' => $request->email,
                'subject' => $request->subject,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            if($res){
                return redirect()->route('contact_page')->with('status', "Your message is sended!");
            }
        }
        return view('templates.contacts', $data);
    }
    public function showlist(){
        $field = 'created_at';
        $order = 'desc';

        if(Input::get('field', false)){
            $field = Input::get('field', false);
            $order = Input::get('order_by', false);
        }

        $order_by = [
            'created_at' => $order,
            'name' => $order,
            'email' => $order,
            'subject' => $order,
            'text' => $order,
        ];


        $all = Contact::orderBy($field, $order)->get();

        if($order_by[$field] == 'desc'){
            $order_by[$field] = 'asc';
        }else{
            $order_by[$field] = 'desc';
        }


        $data = [
            'page_title' => 'Contacts form sends',
            'list' => $all,
            'order_by' => $order_by
        ];


        return view('auth.admin.form_list', $data);
    }

    public function delete_item($id = null){
        $item = Contact::find($id);
        if($item){
            $res = $item->delete();
            if($res){
                return redirect()->route('contacts_list')->with('status', "Deleted form item!");
            }
        }else{
            return redirect()->route('contacts_list');
        }
    }


}
