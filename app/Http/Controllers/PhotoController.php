<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use Storage;
use Image;
use Validator;

class PhotoController extends Controller
{
    //
    public function add(Request $request){
        $img = '';
        $data = [
            'page_title' => 'Add photo',
            'img' => $img
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'body' => 'required',
            'date_f' => 'required',
            'date_l' => 'required',
        ]);


        if($request->isMethod('post')){


            if ($validator->fails()) {
                return redirect()->route('photo_add')
                    ->withErrors($validator)
                    ->withInput();
            }

            $photo = new Photo;
            $photo->name = $request->name;
            $photo->body = $request->body;
            $photo->date_f = $request->date_f;
            $photo->date_l = $request->date_l;
            if($request->hasFile('img')){
                $img = $request->file('img');
                $filename = time() .'.'. $img->getClientOriginalExtension();
                $location = public_path('images/' . $filename);
                Image::make($img)->resize(35, 35)->save($location);
                $photo->img = $filename;
            }
            $res = $photo->save();
            if($res){
                return redirect()->route('photo_list')->with('status', 'Added!!!');
            }
        }

        return view('auth.admin.photo_add', $data);
    }

    public function all(){
        $photos = Photo::all();
        $data = [
            'page_title' => 'All photographers',
            'photos' => $photos

        ];
        return view('auth.admin.photos_list', $data);
    }

    public function edit($id = null, Request $request){
       if(Photo::find($id)){
           $current = Photo::find($id);

           $data = [
               'page_title'=> 'Edit item',
               'item' => $current
           ];

           if($request->isMethod('post')){
               $rullers = [
                   'img' => 'sometimes|image',
                   'name' => 'required',
                   'date_f' => 'date|required',
                   'date_l' => 'date|required',
                   'body' => 'required',
               ];
               $validator = Validator::make($request->all(), $rullers);

               if ($validator->fails()) {
                   return redirect()->route('photo_edit', array($id))
                       ->withErrors($validator)
                       ->withInput();
               }

               $current->name = $request->name;
               $current->date_f = $request->date_f;
               $current->date_l = $request->date_l;
               $current->body = $request->body;

               if($request->hasFile('img')){
                   $img = $request->file('img');
                   $filename = time() .'.'. $img->getClientOriginalExtension();
                   $location = public_path('images/' . $filename);
                   Image::make($img)->resize(35, 35)->save($location);

                   $oldFilename = $current->img;
                   if($oldFilename){
                       Storage::delete($oldFilename);
                   }
                   $current->img = $filename;
               }
               $current->save();
               return redirect()->route('photo_list')->with('status', 'Updated');
           }
           return view('auth.admin.photo_edit', $data);
       }
    }

    public function delete($id){

        $current = Photo::find($id);
        if($current){
            if($current->img){
                Storage::delete($current->img);
            }
            $res = $current->delete();
            if($res){
                return redirect()->route('photo_list')->with('status', 'Item Deleted');
            }
        }else{
            abort(404);
        }

    }
}
