<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Validator;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    public function all(){
        $categories = Category::paginate(10);
        $data = [
            'page_title' => 'Edit Categories',
            'categories' => $categories
        ];
        return view('auth.admin.category_list', $data);
    }

    public function show($alias){
        $cat = Category::where('alias', $alias)->get()->first();
        if($cat){
           $data = [
               'page_title' => $cat->name,
               'category'  => $cat,
               'limited'    => 1,
               'articles' => $cat->articles
           ];

           return view('templates.categories', $data);
        }else{
            abort(404);
        }



    }

    public function add(Request $request){
        $data = [
            'page_title' => 'Add category'
        ];

        if($request->isMethod('post')){


            $rule = [
                'name' => 'required',
                'alias' => 'required|unique:categories,alias',
                'image' => 'required',
                'body' => 'required',
            ];

            $validator = Validator::make($request->all(), $rule);

            if ($validator->fails()) {
                return redirect()->route('category_add')
                    ->withErrors($validator)
                    ->withInput();
            }

            $input = $request->except('_token');

            $image = '';
            if($request->hasFile('image')){
                $category = new Category($input);

                $file = $request->file('image');
                $input['image'] = $file->getClientOriginalName();
                $image = $input['image'];
                $file->move(public_path() .'/images', $input['image']);

            }




            $category->name = $input['name'];
            $category->subname = $input['subname'];
            $category->alias = $input['alias'];
            $category->body = $input['body'];
            $category->image = $image;

            if($category->save()){
                return redirect()->route('category_list')
                    ->with('status', "Category was added!");
            }



        }


        return view('auth.admin.category_list_add', $data);
    }

    public function edit(Request $request, $id = null){
        $cat_c = Category::find($id);
        $data = [
            'page_title' => 'Edit category',
            'category'  => $cat_c,
        ];



        if($request->isMethod('post')){

            $rule = [
                'name' => 'required',
                'subname' => 'required',
                'alias' => 'required|unique:categories,alias,'.$request->id,
                'image' => 'image',
                'body' => 'required',
            ];

            $validator = Validator::make($request->all(), $rule);

            if ($validator->fails()) {
                return redirect()->route('category_edit', array('id' => $id))
                    ->withErrors($validator)
                    ->withInput();
            }


            $input = $request->except('_token');

            $category = Category::find($id);

            if($request->hasFile('image')){

                $file = $request->file('image');
                $file->move(public_path().'/images', $file->getClientOriginalName());
                $input['image'] = $file->getClientOriginalName();

            }else{
                $input['image'] = $input['oldimage'];
            }

            $res =  $category->update([
                'name' => $input['name'],
                'subname' => $input['subname'],
                'alias' => $input['alias'],
                'image' => $input['image'],
                'body' => $input['body'],
            ]);

            if($res){
                return redirect()->route('category_list')->with('status', "Category was updated");
            }


        }




        return view('auth.admin.category_list_edit', $data);
    }

    public function delete($id){

        $category = Category::find($id);
        if($category){
            $res =  $category->delete();
            if($res){
                $img_name = $category->image;
                $delete_file_text = '';
                $category_with_image = Category::where('image', $img_name)->get()->first();
                $current_img = $category->image;
                if(!$category_with_image){
                    if(file_exists(public_path().'/images/'.$current_img)){
                        $res_i = unlink(public_path().'/images/'.$current_img);
                        if($res_i){
                            $delete_file_text = ' (and file "'.$current_img.'" was deleted)';
                        }
                    }
                }

                return redirect()->route('category_list')->with('status', "Category was deleted" . $delete_file_text);
            }

        }
    }
}
