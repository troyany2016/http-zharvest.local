<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleTag;
use App\Category;
use App\Tag;
use Validator;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    //
    public function all(){

        $articles = Article::paginate(10);
        $data = [
            'page_title' => 'Articles',
            'list' => $articles,
        ];
        return view('auth.admin.articles_list', $data);
    }
    public function add(Request $request){
        $tags = [];
        $tags_r =  Tag::all();
        foreach ($tags_r as $tag) {
            $ch = 0;
            $tags[$tag->id]['checked'] = $ch;
            $tags[$tag->id]['title'] = $tag->name;
        }
        $cats = [];
        $cats_rs = Category::all();
        if($cats_rs){
            foreach ($cats_rs as $cats_r) {
                $cats[$cats_r->id]['checked'] = 0;
                $cats[$cats_r->id]['name'] = $cats_r->name;
            }
        }



        $data = [
            'page_title' => 'Add new Article',
            'is_edit' => false,
            'tags' => $tags,
            'cats' => $cats
        ];


        if($request->isMethod('post')){

            $rule = [
                'image' => 'required',
                'body' => 'required',
                'alias' => 'required|unique:articles,alias',
                'category' => 'required'
            ];

            $validator = Validator::make($request->all(), $rule);

                if ($validator->fails()) {
                    return redirect()->route('article_form')
                        ->withErrors($validator)
                        ->withInput();
                }

            $input = $request->except('_token');
//            dd($input);

            if($request->hasFile('image')){
                $file = $request->file('image');
                $input['image'] = $file->getClientOriginalName();

                $file->move(public_path() .'/images', $input['image']);
            }
            $article = new Article($input);

            $article->title = $input['title'];
            $article->body = $input['body'];
            $article->img = $input['image'];


            if($article->save()){
                if(!empty($input['category'])){
                    $cat = Category::find($input['category']);
                    $cat->articles()->save($article);
                }

                if(!empty($input['tags'])){
                    foreach ($input['tags'] as $tag_id) {
                        $article->tags()->attach($tag_id);
                        $article->save();
                    }
                }

                return redirect()->route('article_list')
                    ->with('status', "Article '". $article->title ."' was added!");
            }



        }
        return view('auth.admin.article_list_form', $data);
    }
    public function edit(Request $request, $id= null){
        $cArticle = Article::find($id);

        $tags = [];
        $art_tags = Tag::all();
        foreach ($art_tags as $tag) {
            $tags[$tag->id] = [
                'title' => $tag->name,
                'checked' => 0
            ];
        }
        $c_tags = $cArticle->tags;

        if($c_tags){
            foreach ($c_tags as $c_tag) {
                $tags[$c_tag->id]['checked'] = 1;
            }
        }

        $cats = [];
        $cats_rs = Category::all();
        $c_category = $cArticle->category;

        if($cats_rs){
            foreach ($cats_rs as $cats_r) {
                $cats[$cats_r->id]['name'] = $cats_r->name;
                if($c_category && ($c_category->id == $cats_r->id)){
                    $ch = 1;
                }else{
                    $ch = 0;
                }
                $cats[$cats_r->id]['checked'] = $ch;
            }
        }



        $data = [
            'page_title' => 'Edit Article',
            'is_edit' => true,
            'carticle' => $cArticle,
            'tags' => $tags,
            'cats' => $cats
        ];

        if($request->isMethod('post')){

            $rule = [
                'image' => 'image',
                'body' => 'required',
                'alias' => 'required|unique:articles,alias,'.$request->id,
                'title' => 'required',
                'category' => 'required'
            ];

            $validator = Validator::make($request->all(), $rule);
            if ($validator->fails()) {
                return redirect()->route('article_edit', array('id' => $id))
                    ->withErrors($validator)
                    ->withInput();
            }

            $input = $request->except('_token');
            if($request->hasFile('image')){
                $file = $request->file('image');
                $file->move(public_path().'/images', $file->getClientOriginalName());
                $input['image'] = $file->getClientOriginalName();
            }else{
                $input['image'] = $input['oldimage'];
            }




            $article = Article::find($id);
            $tgs = [];
            if(!empty($input['tags'])){
                $tgs = $input['tags'];
            }
            if(!empty($input['category'])){
                $cat = Category::find($input['category']);
                $cat->articles()->save($article);
            }

            $article->tags()->sync($tgs);



            $article->update([
                'title' => $input['title'],
                'alias' => $input['alias'],
                'body' => $input['body'],
                'img' => $input['image'],
            ]);

            if($article){
                return redirect()->route('article_list')->with('status', "Article was updated");
            }

        }


        return view('auth.admin.article_list_form', $data);
    }

    public function allfrontend(){
        $articles = Article::paginate(3);

        $data = [
            'page_title' => 'Articles',
            'list'       => $articles,
            'limited'    => 1,
        ];


        return view('templates.single_list', $data);

    }
    public function show($alias){
        if(is_numeric($alias) && preg_match('/api/', $_SERVER['REQUEST_URI'])){
           $id = $alias;

            $Article = Article::find($id);
//            dd($Article);
            return response()->json($Article);
        }


        $Carticle = Article::where('alias', $alias)->get()->first();
        $breadcrumbs = [
            'Articles' => route('allfrontend'),
        ];
        $date = strtotime($Carticle->created_at);
        $new_date = date('M d, Y', $date);

        $data = [
            'page_title' => $Carticle->title,
            'body'       => $Carticle->body,
            'img'        => $Carticle->img,
            'breadcrumbs'=> $breadcrumbs,
            'limited'    => 1,
            'date'       => $new_date,
            'id'         => $Carticle->id
        ];

        return view('templates.single', $data);
    }
    public function delete($id){
        $delete = Article::find($id);
        if($delete){
            $img_name = $delete->img;
            $tags_r = $delete->tags;
            foreach ($tags_r as $tg) {
                $delete->tags()->detach($tg->id);
            }
            $delete->delete();

            $article_with_img = Article::where('img', $img_name)->get()->first();
            $current_img = $delete->img;
            $delete_file_text = '';
            if(!$article_with_img){
                if(file_exists(public_path().'/images/'.$current_img)){
                    $res = unlink(public_path().'/images/'.$current_img);
                    if($res){
                        $delete_file_text = ' (and file "'.$current_img.'" was deleted)';
                    }

                }
            }

            if($delete){
                return redirect()->route('article_list')->with('status', 'Article was deleted'.$delete_file_text);
            }

        }else{
            return redirect()->route('article_list');
        }


    }

    //------resourse
    public function index(){
        $articles = array();
        $articles['articles'] = Article::all();
        $categories = Category::all();
        $articles['categories'] = $categories;
        return response()->json($articles);
    }

    public function store(Request $request){
        $rule = [
            'body' => 'required',
            'alias' => 'required|unique:articles,alias,'.$request->id,
            'title' => 'required',
            'category' => 'required',
        ];

        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            $response = array('response' => $validator->messages(), 'success' => false);
            return $response;
        }else{
            $article = new Article;
            $article->title = $request->input('title');
            $article->body = $request->input('body');
            $article->alias = $request->input('alias');
            $article->category_id = $request->input('category');
            $article->img = '6.jpg';
            $article->save();
            return response()->json($article);
        }

    }

    public function update(Request $request, $id){
        $rule = [
            'body' => 'required',
            'alias' => 'required|unique:articles,alias,'.$id,
            'title' => 'required',
            'category' => 'required'
        ];

        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            $response = array('response' => $validator->messages(), 'success' => false);
            return $response;
        }else{
            $article =Article::find($id);
            $article->title = $request->input('title');
            $article->body = $request->input('body');
            $article->alias = $request->input('alias');
            $article->category_id = $request->input('category');
            $article->img = '';
            $article->save();
            return response()->json($article);
        }
    }

    public function destroy($id){
        $delete = Article::find($id);

        if($delete){
            $img_name = $delete->img;
            $tags_r = $delete->tags;
            foreach ($tags_r as $tg) {
                $delete->tags()->detach($tg->id);
            }
            $delete->delete();

            $article_with_img = Article::where('img', $img_name)->get()->first();
            $current_img = $delete->img;
            if(!$article_with_img){
                if(file_exists(public_path().'/images/'.$current_img)){
                   unlink(public_path().'/images/'.$current_img);
                }
            }
            $response = array('response' => 'deleted', 'success' => true);
        }else{
            $response = array('response' => 'not deleted', 'success' => false);
        }

        return $response;


    }

}
