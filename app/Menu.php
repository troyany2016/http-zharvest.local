<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $fillable = ['weight', 'parent', 'link', 'name'];
    public static function get_children($id){
        $children = [];
        $get_children = Menu::where('parent', $id)->get();
        if(count($get_children)){
            foreach ($get_children as $child) {
                $children[] = [
                    'link' => $child->link,
                    'name' => $child->name,
                    'weight' => $child->weight,
                    'id' => $child->id
                ];
            }
        }
        return $children;
    }

    public static function get_first_level(){
        $top_level = Menu::where('parent', 0)->orderBy('weight', 'asc')->get();
        $top_items = [];
        if(count($top_level)){
            foreach ($top_level as $item) {
                $top_items[] = [
                    'link' => $item->link,
                    'name' => $item->name,
                    'weight' => $item->weight,
                    'id' => $item->id
                ];
            }
        }
        return $top_items;
    }

}
