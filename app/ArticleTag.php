<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleTag extends Model
{
    //
    protected $table = 'article_tag';

    protected $fillable = ['tag_id', 'article_id'];
}
