<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//list
Route::get('pages', 'PageController@index');

//single
Route::get('page/{id}', 'PageController@show');

//Create
Route::post('page', 'PageController@store');

//Update
Route::put('page', 'PageController@index');

//destroy
Route::delete('page/{id}', 'PageController@destroy');

Route::resource('articles', 'ArticleController');