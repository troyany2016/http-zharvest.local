<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', ['uses' => 'IndexController@show', 'as' => 'front_page']);


Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '6',
        'redirect_uri' => 'http://zharvest.local/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect('http://rest.loc/oauth/authorize?'.$query);
});

Route::get('/callback', function (Request $request) {
    $http = new \GuzzleHttp\Client;

    $response = $http->post('http://rest.loc/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '6',
            'client_secret' => '8PmaG4M7eE3tFxNACodbEkMDZAT4yZsqqhWJkdX0',
            'redirect_uri' => 'http://zharvest.local/callback',
            'code' => $request->code,
        ],
    ]);
    return json_decode((string) $response->getBody(), true);
});



Route::group(['prefix' => 'admin'], function(){
    Auth::routes();
    Route::get('/', 'HomeController@index')->name('admin_home');

    Route::group(['prefix' => '/texts', 'middleware' => ['web', 'auth']], function(){
        Route::match(['get', 'post'], '/', ['uses' => 'TextController@all', 'as' => 'text_list']);
        Route::match(['get', 'post'], 'edit/{id}', ['uses' => 'TextController@edit', 'as' => 'text_edit']);
        Route::get('delete/{id}', ['uses' => 'TextController@delete', 'as' => 'text_delete']);
    });

    Route::group(['prefix' => '/blocks', 'middleware' => ['web', 'auth']], function(){
        Route::match(['get', 'post'], '/', ['uses' => 'BlockController@all', 'as' => 'block_list']);
        Route::match(['get', 'post'], 'edit/{id}', ['uses' => 'BlockController@edit', 'as' => 'block_edit']);
        Route::get('delete/{id}', ['uses' => 'BlockController@delete', 'as' => 'block_delete']);
    });

    Route::group(['prefix' => '/articles', 'middleware' => ['web', 'auth']], function(){
        Route::match(['get', 'post'], '/', ['uses' => 'ArticleController@all', 'as' => 'article_list']);
        Route::match(['get', 'post'], '/add', ['uses' => 'ArticleController@add', 'as' => 'article_form']);
        Route::match(['get', 'post'], 'edit/{id}', ['uses' => 'ArticleController@edit', 'as' => 'article_edit']);
        Route::get('delete/{id}', ['uses' => 'ArticleController@delete', 'as' => 'article_delete']);
    });
    Route::group(['prefix' => '/tags', 'middleware' => ['web', 'auth']], function(){
        Route::match(['get', 'post'], '/', ['uses' => 'TagController@all', 'as' => 'tag_list']);
        Route::match(['get', 'post'], 'edit/{id}', ['uses' => 'TagController@edit', 'as' => 'tag_edit']);
        Route::get('delete/{id}', ['uses' => 'TagController@delete', 'as' => 'tag_delete']);
    });
    Route::group(['prefix' => '/menu', 'middleware' => ['web', 'auth']], function(){
        Route::match(['get', 'post'], '/', ['uses' => 'MenuController@all', 'as' => 'menu_list']);
        Route::match(['get', 'post'], '/edit/{id}', ['uses' => 'MenuController@edit', 'as' => 'menu_edit']);
    });
    Route::get('/contacts', ['uses' => 'ContactController@showlist','middleware' => ['web', 'auth'], 'as' => 'contacts_list']);
    Route::get('/contacts/delete/{id}', ['uses' => 'ContactController@delete_item','middleware' => ['web', 'auth'], 'as' => 'contacts_delete']);

    Route::group(['prefix' => '/categories', 'middleware' => ['web', 'auth']], function(){
        Route::get('/', ['uses' => 'CategoryController@all', 'as' => 'category_list']);
        Route::get('/delete/{id}', ['uses' => 'CategoryController@delete', 'as' => 'category_delete']);
        Route::match(['get', 'post'], '/new', ['uses' => 'CategoryController@add', 'as' => 'category_add']);
        Route::match(['get', 'post'], '/edit/{id}', ['uses' => 'CategoryController@edit', 'as' => 'category_edit']);
    });
    Route::group(['prefix' => '/photos', 'middleware' => ['web', 'auth']], function(){
        Route::match(['get', 'post'], '/new', ['uses' => 'PhotoController@add', 'as' => 'photo_add']);
        Route::get('/', ['uses' => 'PhotoController@all', 'as' => 'photo_list']);
        Route::match(['get', 'post'], '/edit/{id}', ['uses' => 'PhotoController@edit', 'as' => 'photo_edit']);
        Route::get('/edit/{id}/delete', ['uses' => 'PhotoController@delete', 'as' => 'photo_delete']);
    });

});

Route::group(['prefix' => '/articles'], function(){
    Route::get('/', ['uses' => 'ArticleController@allfrontend', 'as' => 'allfrontend']);
    Route::get('/{alias}', ['uses' => 'ArticleController@show', 'as' => 'show']);
});

Route::match(['get', 'post'], '/contacts', ['uses' => 'ContactController@show', 'as' => 'contact_page']);

Route::get('/{alias}', ['uses' => 'CategoryController@show', 'as' => 'show_cat']);
