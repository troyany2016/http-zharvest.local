<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       DB::table('blocks')->insert([
        [
            'blockid' => 'text1',
            'text' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing 
                elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore 
                magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud 
                exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea 
                commodo consequat. Duis autem vel eum iriure dolor in hendrerit in.',
        ],
           [
               'blockid' => 'text2',
               'text' => '2Lorem ipsum dolor sit amet, consectetuer adipiscing 
                elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore 
                magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud 
                exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea 
                commodo consequat. Duis autem vel eum iriure dolor in hendrerit in.',
           ],

       ]);
    }
}
