( function( $ ) {
$( document ).ready(function() {
$('#cssmenu').prepend('<div id="menu-button">Menu</div>');
	$('#cssmenu #menu-button').on('click', function(){
		var menu = $(this).next('ul');
		if (menu.hasClass('open')) {
			menu.removeClass('open');
		}
		else {
			menu.addClass('open');
		}
	});
	if($('#textblock').length){
        $('#textblock').summernote({
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],

            ]
        });
	}
	if($('.multipleselect').length){
        $('.multipleselect').select2();
	}
	if($('.dd').length){
        var json = $('#nestable2-output').val();

        var updateOutput = function(e)
        {
            var list   = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        $('#nestable').nestable({
            json: json,
            maxDepth: 3
        }).on('change', updateOutput);
        updateOutput($('#nestable').data('output', $('#nestable2-output')));
	}

	$('.dd-item a').mousedown(function(){
        window.location.href = $(this).attr('href');
    })


    $('#contact_form .button.button-skin').click(function(e){
        e.preventDefault();
        $('.alert.alert-success').remove();

        var _token = $("input[name='_token']").val();
        var name = $("input[name='name']").val();
        var email = $("input[name='email']").val();
        var subject = $("input[name='subject']").val();
        var message = $("textarea[name='message']").val();


        $.ajax({
            url: "/contacts",
            type:'POST',
            data: {_token:_token, name:name, message:message, email:email, subject:subject},
            success: function(data) {
                if($.isEmptyObject(data.error)){
                    $(".print-error-msg").css('display','none');
                    $(".print-error-msg").find("ul").html('');
                    $('#contact_form').prepend('<div class="alert alert-success" role="alert">\n' +
                        data.success +
                        '</div>');
                    $("input[name='name']").val('');
                    $("input[name='email']").val('');
                    $("input[name='subject']").val('');
                    $("textarea[name='message']").val('');
                }else{
                    printErrorMsg(data.error);
                }
            }
        });

    });

    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
    //
    // var form = document.photo_add;
    // var send = form.send_ajax;
    //
    //
    // function mm(e) {
    //     e.preventDefault();
    //
    //     //values
    //     var _token = form._token.value;
    //     var name = form.name.value;
    //
    //     var xhr = new XMLHttpRequest();
    //     var data = {
    //         name: name,
    //         _token: _token
    //     };
    //
    //
    // }
    //
    // send.addEventListener('click', mm, false);









});
} )( jQuery );
