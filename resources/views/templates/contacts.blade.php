@extends('base')
@section('wc')
    <div class="crumbs">
        @include('includes.breadcrumbs')
    </div>
    @if (count($errors->all()))
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">{{$error}}</div>
        @endforeach
    @endif

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div id="main-content">
        <div class="wrap-content " style="border-right: none">
            <div class="row">
                <h1 class="t-center" style="margin: 40px 0;color: #212121;letter-spacing: 2px;font-weight: 500;">{{$page_title}}</h1>
                <div class="col-full">
                    <div id="map" style="height: 450px; position: relative; overflow: hidden;"></div>
                </div>
                <div class="col-1-3">
                    <div class="wrap-col">
                        <h3 style="margin: 20px 0">{!! t('Contact_Info') !!}</h3>
                       {!! block('contact') !!}
                    </div>
                </div>
                <div class="col-2-3">
                    <div class="wrap-col">
                        <div class="contact">
                            <h3 style="margin: 20px 0 20px 30px">{!! t('Contact_Form') !!}</h3>
                            <div id="contact_form">
                                <div class="alert alert-danger print-error-msg" style="display:none">
                                    <ul></ul>
                                </div>

                                {!! Form::open(['route' => 'contact_page', 'id' => 'ff', 'name' => 'form1']) !!}
                                    <div class="row">
                                        <div class="col-1-2">
                                            <div class="wrap-col">
                                                {!! Form::text('name', old('name'), ['placeholder' => 'Enter name', 'id' => 'name']) !!}
                                            </div>
                                        </div>
                                        <div class="col-1-2">
                                            <div class="wrap-col">
                                                {!! Form::text('email', old('email'), ['placeholder' => 'Enter email', 'id' => 'email']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-full">
                                            <div class="wrap-col">
                                                {!! Form::text('subject', old('subject'), ['placeholder' => 'subject', 'id' => 'subject']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="wrap-col">
                                            {!! Form::textarea('message', old('message'), ['placeholder' => 'message', 'id' => 'message', 'rows'=> "4", 'cols' => "25"]) !!}
                                        </div>
                                    </div>
                                    <center>
                                        {!! Form::submit('Submit', ['class' => 'button button-skin', 'name' => 'submitcontact']); !!}
                                    </center>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection