@extends('base')

@section('header')
  @parent
@endsection

@section('wc')

<!-----------------content-box-2-------------------->
<section class="content-box boxstyle-2 box-2">
    <div class="zerogrid">
        <div class="row wrap-box"><!--Start Box-->
            <div class="row">
                <div class="col-2-3 offset-1-6">
                    <div class="header">
                        <div class="wrapper">
                            <div class="intro"> {!! t('text3') !!}</div>
                            <h2 class="color-yellow"> {!! t('text4') !!}</h2>
                        </div>
                    </div>
                    <p class="lead">{!! t('text5') !!}</p>
                    <div class="row">
                        <div class="col-1-2">
                            <div class="wrap-col" style="text-align: justify;">
                                {!! block('text1') !!}
                            </div>
                        </div>
                        <div class="col-1-2">
                            <div class="wrap-col" style="text-align: justify;">
                                {!! block('text2') !!}
                            </div>
                        </div>
                    </div>
                    <blockquote><p>{!! t('text6') !!}</p></blockquote>
                </div>
            </div>

            @if(count($front_articles))

                <div id="owl-slide" class="owl-carousel">
                    @foreach($front_articles as $article)

                        <div class="item">
                            <img src="{{'/images/'.$article->img}}" />
                            <div class="carousel-caption">
                                <div class="carousel-caption-inner">
                                    <p class="carousel-caption-title"><a href="{{route('show', array('alias' => $article->alias))}}">{{$article->title}}</a></p>
                                    @if($article->tags)
                                        <p class="carousel-caption-category">
                                            @php ($kk = 0)
                                            @php ($arr_c = count($article->tags))
                                            @foreach($article->tags as $tag)
                                                @php ($kk = $kk+1)
                                                <a href="#" rel="category tag">{{$tag->name}}</a>
                                                @if($arr_c > $kk)
                                                    ,
                                                @endif
                                            @endforeach

                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</section>

<!-----------------content-box-3-------------------->
@include('includes.categories_block')
<!-----------------content-box-4-------------------->
@include('includes.photograph_block')

@endsection
