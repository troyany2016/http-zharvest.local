@extends('base')
@section('wc')
    <div class="crumbs">
        @include('includes.breadcrumbs')
    </div>
    <div id="main-content">
        <div class="wrap-content">
            <article>
                @if(Auth::check())
                    <div class="admin_edit">
                        <div class="admin_edit_link"><a href="{{route('article_edit', array($id))}}" class="edit_block">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </a>
                        </div>
                        @endif
                            <div class="art-header">
                                <div class="entry-title">
                                    <h2>{{$page_title}}</h2>
                                </div>
                                <div class="info">{{$date}}</div>
                            </div>
                            <div class="art-content">
                                <div class="img_wrapp">
                                    <img src="/images/{{$img}}" alt="">
                                </div>
                                {!! $body !!}
                            </div>
                @if(Auth::check())
                    </div> <!-- <div class="admin_edit"> -->
                @endif
            </article>
        </div>
    </div>
@endsection