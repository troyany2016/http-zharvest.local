@extends('base')
@section('wc')
    <div class="crumbs">
        @include('includes.breadcrumbs')
    </div>
    @if (count($errors->all()))
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">{{$error}}</div>
        @endforeach
    @endif

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div id="main-content">
        <div class="wrap-content " style="border-right: none">
            <div class="row">
                <h1 class="t-center" style="margin: 40px 0;color: #212121;letter-spacing: 2px;font-weight: 500;">{{$page_title}}</h1>
                <div class="clearfix" style="margin-bottom:40px">
                    @if(Auth::check())
                        <div class="admin_edit">
                            <div class="admin_edit_link"><a href="{{route('category_edit', array($category->id))}}" class="edit_block">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </div>
                    @endif

                        @if($category->image)
                            <img src="/images/{{$category->image}}" alt="" style="float:left;;max-width:300px; margin-right:10px">
                        @endif
                        @if($category->body)
                            {!! $category->body !!}
                        @endif

                    @if(Auth::check())
                        </div> <!-- <div class="admin_edit"> -->
                    @endif
                </div>
                @if($articles)
                    @foreach($articles as $item)
                        <article>
                            <div class="post-inner">
                                <figure>
                                    <a href="{{route('show', array($item->alias))}}">
                                        <img src="/images/{{$item->img}}" alt="">
                                    </a>
                                </figure>
                                <div class="post-content">
                                    <div class="entry-header">
                                        <h2 class="entry-title">
                                            <a href="{{route('show', array($item->alias))}}">{{$item->title}}</a>
                                        </h2>
                                        <div class="entry-meta">
                                            <span style="color: #eee;"><i class="fa fa-calendar"></i> {{date('F d, Y', strtotime($item->created_at))}}</span>
                                            @if(count($item->tags))
                                                @foreach($item->tags as $tag)
                                                    <span style="color: #eee;"><i class="fa fa-tag"></i> {{$tag->name}}</span>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="entry-content">
                                        <p>{{str_limit(strip_tags($item->body), 180)}}</p>
                                        <a href="{{route('show', array($item->alias))}}" class="button button-skin">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </article>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
@endsection