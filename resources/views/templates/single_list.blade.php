@extends('base')
@section('wc')
    <div class="crumbs">
        @include('includes.breadcrumbs')
    </div>
    <div id="main-content">
        <div class="wrap-content">

            @if(isset($list) && count($list))
                @foreach($list as $item)

                    <article>
                        <div class="post-inner">
                            <figure>
                                <a href="{{route('show', array($item->alias))}}">
                                    <img src="/images/{{$item->img}}" alt="">
                                </a>
                            </figure>
                            <div class="post-content">
                                <div class="entry-header">
                                    <h2 class="entry-title">
                                        <a href="{{route('show', array($item->alias))}}">{{$item->title}}</a>
                                    </h2>
                                    <div class="entry-meta">
                                        <span style="color: #eee;"><i class="fa fa-calendar"></i> {{date('F d, Y', strtotime($item->created_at))}}</span>
                                        @if(count($item->tags))
                                            @foreach($item->tags as $tag)
                                                <span style="color: #eee;"><i class="fa fa-tag"></i> {{$tag->name}}</span>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="entry-content">
                                    <p>{{str_limit(strip_tags($item->body), 180)}}</p>
                                    <a href="{{route('show', array($item->alias))}}" class="button button-skin">Read More</a>
                                </div>
                            </div>
                        </div>
                    </article>
                @endforeach
                {{ $list->links() }}
            @endif

        </div>
    </div>

@endsection