<div class="col-md-3">
    <div class="list-group">
        <a href="{{route('admin_home')}}" class="list-group-item">Info</a>
        <a href="{{route('text_list')}}" class="list-group-item">Edit texts</a>
        <a href="{{route('block_list')}}" class="list-group-item">Edit blocks</a>
        <a href="{{route('article_list')}}" class="list-group-item">Edit articles</a>
        <a href="{{route('tag_list')}}" class="list-group-item">Edit tags</a>
        <a href="{{route('menu_list')}}" class="list-group-item">Edit menu</a>
        <a href="{{route('category_list')}}" class="list-group-item">Edit Categories</a>
        <a href="{{route('photo_list')}}" class="list-group-item">Edit Photografs</a>
        <a href="{{route('contacts_list')}}" class="list-group-item">Contacts form</a>
    </div>
</div>