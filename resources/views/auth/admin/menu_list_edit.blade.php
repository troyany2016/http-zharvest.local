@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('auth.admin.errors')
                            <form method="post" action="{{route('menu_edit', array('id' => $item->id))}}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Menu Name</label>
                                            <input type="text" name="name" class="form-control"  value="@if(old('name')){{old('name')}}@else{{$item->name}}@endif" placeholder="Menu name">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Menu Link</label>
                                            <input type="text" name="link" class="form-control" value="@if(old('link')){{old('link')}}@else{{$item->link}}@endif" placeholder="Menu link">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label style="display:block">&nbsp;</label>
                                        <button type="submit" name="add_link" value="Create new link" class="btn btn-success">Edit Link</button>
                                    </div>
                                </div>
                            </form>
                            <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
