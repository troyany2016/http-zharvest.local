@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('auth.admin.errors')

                            <form action="@if($is_edit){{route('article_edit', $carticle->id)}}@else{{route('article_form')}}@endif" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" value="@if(old('title')){{old('title')}}@elseif($is_edit){{$carticle->title}}@endif" name="title">
                                </div>
                                <div class="form-group">
                                    <label>Alias</label>
                                    <input type="text" class="form-control" value="@if(old('alias')){{old('alias')}}@elseif($is_edit){{$carticle->alias}}@endif" name="alias">
                                </div>
                                <div class="form-group">
                                    @if(count($cats))

                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Category</label>
                                            <select class="form-control" id="exampleFormControlSelect1" name="category">
                                                <option value="">Select category</option>
                                                @foreach($cats as $kk => $cat)
                                                    <option value="{{$kk}}"
                                                        @if(old('category') == $kk)
                                                            selected
                                                        @elseif($cat['checked'] == 1)
                                                            selected
                                                        @endif
                                                    >
                                                        {{$cat['name']}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                    @endif

                                </div>

                                <div class="form-group">

                                    @if($is_edit)
                                        @if($carticle->img)

                                            <img src="/images/{{$carticle->img}}" alt="">
                                            <input type="hidden" name="oldimage" value="{{$carticle->img}}">
                                        @endif
                                    @endif
                                    <label for="file">Image</label>
                                    <input type="file" class="form-control-file" id="file" name="image">
                                </div>

                                <div class="form-group">
                                    <label for="textblock">Text Article</label>
                                    <textarea class="form-control" id="textblock" name="body" rows="3">@if(old('body')){{old('body')}}@elseif($is_edit){{$carticle->body}}@endif</textarea>
                                </div>

                                {{--@if($is_edit)--}}

                                    @if(count($tags))

                                        <div class="form-group">
                                            <label for="textblock">tags</label>
                                            <select multiple size="10" name="tags[]" class="multipleselect" style="width:100%">
                                                @foreach($tags as $key=>$tag)
                                                    <option value="{{$key}}"
                                                            @if(old('tags'))
                                                                @foreach(old('tags') as $oldt)
                                                                    @if($oldt == $key)
                                                                        selected
                                                                    @endif
                                                                @endforeach
                                                            @elseif($tag['checked'])
                                                                selected
                                                            @endif
                                                    >{{$tag['title']}}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    @endif
                                {{--@endif--}}
                                <div class="form-group">
                                    <input class="btn btn-success" type="submit">
                                </div>

                            </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection