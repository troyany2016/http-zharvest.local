@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('auth.admin.errors')
                        <form method="post" action="{{route('block_edit', array($block_id))}}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="textid">Block ID</label>
                                        <input type="text" name="blockid" class="form-control" id="textid" value="@if($bTextId){{$bTextId}}@endif" placeholder="Text">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="simple_text">Text</label>
                                        <textarea name="text" id="textblock" class="form-control" cols="30" rows="10" id="text">@if(old('text'))  {{old('text')}} @else {{$bText}}@endif</textarea>
                                    </div>

                                    <div class="col-md-4">
                                        <label style="display: block">&nbsp;</label>
                                        <button type="submit" class="btn btn-primary">Change text</button>
                                    </div>

                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
