@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('auth.admin.errors')

                            <form method="post" action="{{route('category_add')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <label for="textid">Category name</label>
                                            <input type="text" name="name" class="form-control" id="textid" value="{{old('name')}}" placeholder="Category name">
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <label for="textid">Category subname</label>
                                            <input type="text" name="subname" class="form-control" id="textid" value="{{old('subname')}}" placeholder="Category name">
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <label for="textid">Category link</label>
                                            <input type="text" name="alias" class="form-control" id="textid" value="{{old('alias')}}" placeholder="alias">
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">

                                            {{--@if($is_edit)--}}
                                                {{--@if($carticle->img)--}}

                                                    {{--<img src="/images/{{$carticle->img}}" alt="">--}}
                                                    {{--<input type="hidden" name="oldimage" value="{{$carticle->img}}">--}}
                                                {{--@endif--}}
                                            {{--@endif--}}

                                            <label for="file">Image</label>
                                            <input type="file" class="form-control-file" id="file" name="image">
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <label for="simple_text">Body</label>
                                            <textarea name="body" id="textblock" class="form-control" cols="30" rows="10" id="text">{{old('body')}}</textarea>
                                        </div>
                                        <div class="col-md-4">
                                            <label style="display: block">&nbsp;</label>
                                            <button type="submit" class="btn btn-primary">Add category</button>
                                        </div>

                                    </div>
                                </div>
                            </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection