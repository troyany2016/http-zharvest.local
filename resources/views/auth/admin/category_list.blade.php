@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('auth.admin.errors')
                    <div class="row">
                        <div class="col-md-12"><a href="{{route('category_add')}}" class="btn btn-primary btn-lg">Add new category</a></div>
                    </div>

                        @if(count($categories))
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Desc</th>
                                        <th scope="col">Articles</th>
                                        <th scope="col">Operaations</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $category)
                                            <tr>
                                                <td><a href="{{route('show_cat', array($category->alias))}}">{{$category->name}}</a></td>
                                                <td>{!! $category->body !!}</td>
                                                <td>
                                                    @if($category->articles)

                                                        @foreach($category->articles as $article)
                                                            <a href="{{route('show', array('alias' => $article->alias))}}">{{$article->title}}</a>
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{route('category_edit', array('id' => $category->id))}}" class="btn btn-danger btn-sm">Edit</a>
                                                    <a href="{{route('category_delete', array('id' => $category->id))}}" class="btn btn-primary btn-sm">Delete</a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="text" style="text-align: center; padding: 20px 30px;">
                                    No categories! Add new category!
                                </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection