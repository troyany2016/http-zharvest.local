@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('auth.admin.errors')

                        @if(!empty($list))

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col"><a href="{{route('contacts_list', ['field' => 'created_at', 'order_by' => $order_by['created_at']])}}">Date</a></th>
                                        <th scope="col"><a href="{{route('contacts_list', ['field' => 'name', 'order_by' => $order_by['name']])}}">Name</a></th>
                                        <th scope="col"><a href="{{route('contacts_list', ['field' => 'email', 'order_by' => $order_by['email']])}}">email</a></th>
                                        <th scope="col"><a href="{{route('contacts_list', ['field' => 'subject', 'order_by' => $order_by['subject']])}}">subject</a></th>
                                        <th scope="col"><a href="{{route('contacts_list', ['field' => 'text', 'order_by' => $order_by['text']])}}">text</a></th>
                                        <th scope="col">Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($list as $item)
                                            <tr>
                                                <td>{{$item->created_at}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->email}}</td>
                                                <td>{{$item->subject}}</td>
                                                <td>{{$item->text}}</td>
                                                <td>
                                                    <a href="{{route('contacts_delete', array('id' => $item->id))}}">
                                                        <button type="button" class="btn btn-danger btn-sm">Delete</button>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
