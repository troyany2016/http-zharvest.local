@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('auth.admin.errors')

                            <form method="post" action="{{route('photo_add')}}" enctype="multipart/form-data" name="photo_add">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <label for="textid">Photo name</label>
                                            <input type="text" name="name" class="form-control" id="textid" value="{{old('name')}}" placeholder="Photo name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-6">
                                            <label>Date 1</label>
                                            <input name="date_f" placeholder="2018-02-14" class="form-control" value="{{old('date_f')}}">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Date 2</label>
                                            <input name="date_l" placeholder="2018-02-14" class="form-control" value="{{old('date_l')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <input type="file" name="img">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                              <textarea id="textblock" name="body" rows="3" class="form-control" >{{old('body')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <input type="submit" value="Send" class="btn btn-primary" name="send_ajax">
                                        </div>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection