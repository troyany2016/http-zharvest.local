@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('auth.admin.errors')
                        <form method="post" action="{{route('tag_edit', array('id'=> $tag->id))}}" style="margin-bottom:30px">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <label for="textid">Tag</label>
                                        <input type="text" name="tag" class="form-control" id="textid" value="@if(old('tag'))@else{{$tag->name}}@endif{{old('tag')}}" placeholder="Edit Tag">
                                    </div>

                                    <div class="col-md-4">
                                        <label style="display: block">&nbsp;</label>
                                        <button type="submit" class="btn btn-primary">Edit tag</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
