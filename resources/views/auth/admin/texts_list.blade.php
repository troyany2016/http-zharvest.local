@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('auth.admin.errors')

                        <form method="post" action="{{route('text_list')}}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Text</label>
                                        <input type="text" name="simpletext" class="form-control" id="simple_text" value="" placeholder="Add Text">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Text ID</label>
                                        <input type="text" name="textid" class="form-control" id="textid" value="{{ request()->has('textid') ? request()->get('textid') : '' }}" placeholder="Text id">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label style="display:block">&nbsp;</label>
                                    <button type="submit" class="btn btn-success">Add Text</button>
                                </div>
                            </div>


                        </form>

                        @if(count($text_list))

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">text</th>
                                        <th scope="col">operations</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($text_list as $item)
                                        <tr>
                                            <th scope="row">{{$item->textid}}</th>
                                            <th scope="row">{{$item->simpletext}}</th>
                                            <th scope="row">
                                                <a href="{{route('text_edit', array($item->id))}}" class="btn btn-sm btn-info">Edit</a>
                                                <a href="{{route('text_delete', array($item->id))}}" class="btn btn-sm btn-danger">Delete</a>
                                            </th>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                        @endif



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection