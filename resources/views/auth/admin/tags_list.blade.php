@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('auth.admin.errors')
                        <form method="post" action="{{route('tag_list')}}" style="margin-bottom:30px">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <label for="textid">Tag</label>
                                        <input type="text" name="tag" class="form-control" id="textid" value="" placeholder="New Tag">
                                    </div>

                                    <div class="col-md-4">
                                        <label style="display: block">&nbsp;</label>
                                        <button type="submit" class="btn btn-primary">Add tag</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-12">
                                @if(count($tags))
                                    @foreach($tags as $tag)

                                        <span class="btn btn-primary"><a href="{{route('tag_edit', array('id' => $tag))}}" title="edit tag" style="margin-right:5px; color:#fff"><i class="fa fa-pencil" aria-hidden="true"></i></a>{{$tag->name}}<a href="{{route('tag_delete', array('id' => $tag))}}" style="margin-left:5px; color:#fff" title="delete tag"><i class="fa fa-trash" aria-hidden="true"></i></a></span>
                                    @endforeach
                                    @else
                                    <div class="col-md-12">
                                        <div class="text" style="text-align:center; padding:20px 30px;">
                                            No articles! Add new article!
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
