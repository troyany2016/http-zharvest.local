@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('auth.admin.errors')
                            <form method="post" action="{{route('text_edit', array($text_id))}}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label for="simple_text">Text</label>
                                            <input type="text" name="simpletext" class="form-control" id="simple_text" value="@if($cText){{$cText}}@endif" placeholder="Text">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="textid">Text ID</label>
                                            <input type="text" name="textid" class="form-control" id="textid" value="@if($cTextId){{$cTextId}}@endif" placeholder="Text">
                                        </div>
                                        <div class="col-md-4">
                                            <label style="display: block">&nbsp;</label>
                                            <button type="submit" class="btn btn-primary">Change text</button>
                                        </div>

                                    </div>
                                </div>


                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
