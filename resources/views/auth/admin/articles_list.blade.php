@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('auth.admin.errors')
                            <div class="row" style="margin-bottom:20px">
                                <div class="col-md-12">

                                    <a href="{{route('article_form')}}" class="btn btn-primary btn-lg">Add new article</a>
                                </div>
                            </div>

                        @if(count($list))
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Page title</th>
                                        <th scope="col">Small desc</th>
                                        <th scope="col">Updated</th>
                                        <th scope="col"></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($list as $item)
                                        <tr>
                                            <td><a href="{{route('show', array($item->alias))}}">{{$item->title}}</a></td>
                                            <td>{{str_limit(strip_tags($item->body), 100)}}</td>
                                            <td>{{$item->updated_at}}</td>
                                            <td>
                                                <a href="{{route('article_edit', array('id' => $item->id))}}" class="btn btn-primary btn-sm">EDIT</a>
                                                <a href="{{route('article_delete', array('id' => $item->id))}}" class="btn btn-danger btn-sm">DELETE</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                                {{ $list->links() }}
                            @else
                            <div class="col-md-12">
                                <div class="text" style="text-align:center; padding:20px 30px;">
                                    No articles! Add new article!
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection