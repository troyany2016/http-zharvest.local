@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('auth.admin.errors')

                            <form method="post" action="{{route('photo_edit', array('id' => $item->id))}}" enctype="multipart/form-data" name="photo_add">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <label for="textid">Photo name</label>
                                            <input type="text" name="name" class="form-control" id="textid" value="@if(old('name')){{old('name')}}@else{{$item->name}}@endif" placeholder="Photo name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-6">
                                            <label>Date 1</label>
                                            <input name="date_f" placeholder="2018-02-14" class="form-control" value="@if(old('date_f')){{old('date_f')}}@else{{$item->date_f}}@endif">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Date 2</label>
                                            <input name="date_l" placeholder="2018-02-14" class="form-control" value="@if(old('date_l')){{old('date_l')}}@else{{$item->date_l}}@endif">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <img src="{{asset('images/'.$item->img)}}" alt="">
                                            <input type="file" name="img">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                              <textarea id="textblock" name="body" rows="3" class="form-control" >@if(old('body')){{old('body')}}@else{{$item->body}}@endif</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <input type="submit" value="Send" class="btn btn-primary" name="send_ajax">
                                        </div>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection