@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('auth.admin.errors')
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{route('photo_add')}}" class="btn btn-primary btn-lg">Add new item</a>
                                </div>
                            </div>
                            @if(count($photos))


                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Articles</th>
                                    <th scope="col">Operaations</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--{{dd($photos)}}--}}
                                    @foreach($photos as $photo)
                                        <tr>
                                            <td><img src="{{asset('images/'.$photo->img)}}" alt=""></td>
                                            <td>{{$photo->name}}</td>
                                            <td>{!! $photo->body !!}</td>
                                            <td>
                                                <a href="{{route('photo_edit', array('id' => $photo->id))}}" class="btn btn-primary btn-sm">Edit</a>
                                                <a href="{{route('photo_delete', array('id' => $photo->id))}}" class="btn btn-danger btn-sm">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach



                                </tbody>
                            </table>
                             @else
                                No items
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
