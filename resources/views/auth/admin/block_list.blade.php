@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('auth.admin.errors')

                            <form method="post" action="{{route('block_list')}}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="textid">Block ID</label>
                                            <input type="text" name="blockid" class="form-control" id="textid" value="@if(old('blockid')) {{old('blockid')}}@endif" placeholder="Text">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="simple_text">Text</label>
                                            <textarea name="text" id="textblock" class="form-control" cols="30" rows="10" id="text">@if(old('text')) {{old('text')}}@endif</textarea>
                                        </div>
                                        <div class="col-md-4">
                                            <label style="display: block">&nbsp;</label>
                                            <button type="submit" class="btn btn-primary">Add text</button>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        @if(count($block_list))
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">text block</th>
                                    <th scope="col">operations</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($block_list as $item)
                                        <tr data-id="{{$item->id}}">
                                            <th scope="row">{{$item->blockid}}</th>
                                            <th scope="row">{!! $item->text !!}</th>
                                            <th scope="row">
                                                <a href="{{route('block_edit', array($item->id))}}" id="editblock" class=" btn btn-sm btn-info">Edit</a>
                                                <a href="{{route('block_delete', array($item->id))}}" id="deleteblock" class="btn btn-sm btn-danger">Delete</a>
                                            </th>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                                {{ $block_list->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection