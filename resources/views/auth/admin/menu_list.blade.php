@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @include('auth.admin.lsidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$page_title}}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('auth.admin.errors')

                            <form method="post" action="{{route('menu_list')}}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Menu Name</label>
                                            <input type="text" name="name" class="form-control"  value="{{old('name')}}" placeholder="Menu name">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Menu Link</label>
                                            <input type="text" name="link" class="form-control" value="{{old('link')}}" placeholder="Menu link">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label style="display:block">&nbsp;</label>
                                        <button type="submit" name="add_link" value="Create new link" class="btn btn-success">Add Link</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="dd" id="nestable"></div>
                            <form action="{{route('menu_list')}}" method="post">
                                {{ csrf_field() }}
                                <textarea name="menujson" id="nestable2-output" cols="30" rows="10" style="display:none">{{$json}}</textarea>

                                <input type="submit" name="menu" value="Save position" class="btn btn-primary btn-lg btn-block">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
