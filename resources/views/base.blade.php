<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    @section('head')
        @include('head')
    @show
</head>
<body>
    <div class="wrap-body">
        @section('header')
            @include('includes.header')
        @show
        <section id="container">
            {{--{{dump($name)}}--}}
            <div class="wrap-container @if(isset($limited) && $limited == 1) zerogrid @endif">

                @section('wc')

                @show
            </div>
        </section>
        @section('footer')
            @include('includes.footer')
        @show

        @section('bottom_scripts')
            @include('bottom_scripts')
        @show
    </div>
</body>
</html>