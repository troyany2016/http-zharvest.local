<!-- Google Map -->
<script src="{!! asset('js/google-map.js') !!}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7V-mAjEzzmP6PCQda8To0ZW_o3UOCVCE&callback=initMap" async defer></script>

<!-- carousel -->
<script src="{!! asset('owl-carousel/owl.carousel.js') !!}"></script>
<script>
    $(document).ready(function() {
        $("#owl-slide").owlCarousel({
            autoPlay: false,
            items : 3,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3],
            itemsTablet : [768, 2],
            itemsMobile : [479, 1],
            navigation: true,
            navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            pagination: false
        });
    });
</script>