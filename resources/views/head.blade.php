
<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>zHarvest - Free Html5 Templates</title>
<meta name="description" content="Free Responsive Html5 Css3 Templates | zerotheme.com">
<meta name="author" content="www.zerotheme.com">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="{!! asset('css/zerogrid.css') !!}">
<link rel="stylesheet" href="{!! asset('css/style.css') !!}">
<link rel="stylesheet" href="{!! asset('css/menu.css') !!}">

<!-- Custom Fonts -->
<link href="{!! asset('font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">

<!-- Owl Carousel Assets -->
<link href="{!! asset('owl-carousel/owl.carousel.css') !!}" rel="stylesheet">
<!-- <link href="owl-carousel/owl.theme.css" rel="stylesheet"> -->

<script src="{!! asset('js/jquery-3.1.1.min.js') !!}"></script>
<script src="{!! asset('js/script.js') !!}"></script>

<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
        <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
</div>
<![endif]-->
<!--[if lt IE 9]>
<script src="{!! asset('js/html5.js') !!}"></script>
<script src="{!! asset('js/css3-mediaqueries.js') !!}"></script>
<![endif]-->
