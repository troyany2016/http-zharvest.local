@if(count($photgrafs))
    <section class="content-box boxstyle-1 box-4">
        <div class="zerogrid">
            <div class="row wrap-box"><!--Start Box-->
                <div class="row">
                    @foreach($photgrafs as $photgraf)
                        <div class="col-1-2">
                            <div class="wrap-col">
                                <ul class="history">
                                    @foreach($photgraf as $phot)
                                        <li>
                                            <div class="heading">
                                                <div class="title">{{$phot->name}}</div>
                                                <div class="icon">
                                                    <img src="{{asset('images/'. $phot->img)}}" alt="">
                                                </div>
                                                <div class="datetime">{{date('M Y', strtotime($phot->date_f))}} - {{date('M Y', strtotime($phot->date_l))}}</div>

                                            </div>
                                            <div class="desc">
                                                {!!  $phot->body !!}
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>
@endif