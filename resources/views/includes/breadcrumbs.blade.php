<ul>
    <li><a href="{{route('front_page')}}">Home</a></li>
    @if(isset($breadcrumbs) && count($breadcrumbs))
        @foreach($breadcrumbs as $lname => $breadcrumb_link)
            <li><a href="{{$breadcrumb_link}}">{{$lname}}</a></li>
        @endforeach
    @endif
    <li><a href="#">{{$page_title}}</a></li>
</ul>
