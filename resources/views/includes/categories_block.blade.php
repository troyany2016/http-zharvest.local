@if(count($cats))
    <section class="content-box box-3 ">
        <div class="zerogrid" style="width: 100%;">
            <div class="wrap-box">
                <div class="row">
                    @foreach($cats as $kk => $cat)
                        <div class="col-1-3">
                            <div class="portfolio-box zoom-effect">
                                <img src="/images/{{$cat->image}}"class="img-responsive" alt="">

                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
                                        <div class="project-name">
                                            {{$cat->name}}
                                        </div>
                                        <div class="project-category">
                                            {{$cat->subname}}
                                        </div>
                                        <div class="project-button">
                                            <a href="{{route('show_cat', array('show_cat' => $cat->alias))}}" class="button button-skin">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($kk >= 2)
                            @break
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endif