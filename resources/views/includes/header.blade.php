<header>

    <div class="wrap-header" >
        <!--Top-->
        <div id="top">
            <div class="zerogrid">
                <div class="row">
                    <div class="col-1-3">
                        {!! t('welcome') !!}
                        <span></span>
                    </div>
                    <div class="col-2-3">
                        <ul class="list-inline top-link link">
                            <li><a href="https://www.facebook.com/ZerothemeDotCom/">Facebook</a></li>
                            <li><a href="#">Instagram</a></li>
                            <li><a href="#">Twitter</a></li>
                            <li><a href="#">Google +</a></li>
                            <li><a href="#">Pinterest</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!---Main Header--->
        <div class="main-header">
            <div class="zerogrid">
                <div class="row">
                    <div class="hero-heading">
                        <sup>{!! t('sitename') !!}</sup>
                        <span>{!! t('zharvest') !!}</span>
                        <div class="tl"></div>
                        <div class="tr"></div>
                        <div class="br"></div>
                        <div class="bl"></div>
                    </div>
                </div>
            </div>
        </div>

        <!---Top Menu--->
        <div id="cssmenu" >
            @if(count($main_menu))
                <ul>
                    @foreach($main_menu as $level1)
                        <li class="@if(!empty($level1['children']))has-sub @endif @if($loop->last) last @endif">
                            <a href="{{$level1['link']}}">{{$level1['name']}}</a>
                            @if(!empty($level1['children']))
                                <ul>
                                    @foreach($level1['children'] as $level2)
                                        <li class="@if(!empty($level2['children']))has-sub @if($loop->last) last @endif @endif ">
                                            <a href="{{$level2['link']}}">{{$level2['name']}}</a>
                                            @if(!empty($level2['children']))
                                                <ul>
                                                   @foreach($level2['children'] as $level3)
                                                        <li>
                                                            <a href="{{$level3['link']}}">{{$level3['name']}}</a>
                                                        </li>
                                                   @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</header>